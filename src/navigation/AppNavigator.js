import React from "react";
import Home from '../views/Home';
import {createStackNavigator, createAppContainer} from "react-navigation";
import EstadoHome from "../views/EstadoHome";

const AppNavigator = createStackNavigator({
    Home: Home,
    EstadoHome: EstadoHome
},{
    initialRouteName:"Home",
    headerBackTitleVisible:true,
    headerLayoutPreset:'center',
    headerTransitionPreset:"fade-in-place"
});

export default createAppContainer(AppNavigator);