import {
    AsyncStorage
} from 'react-native';
import Http from "./HttpController";
let AutenticacionController={
    CSRF: async function (callback, callbackError) {

        let url = 'http://192.168.2.215//app-avl/api/csrf.php';

        try {
            let response = await fetch(url, Http.initGET);
            let resp = await response.json();


            if (resp.token) {
                AsyncStorage.setItem("csrf", resp.token);
                AsyncStorage.setItem("PHPSESSID", resp.sid);
                if (callback) {
                    callback(resp);
                }
            } else {
                if (callbackError) {
                    callbackError(resp);
                } else {
                    console.log('no hay token', resp);
                }
            }

        } catch (error) {

            console.log(error);

            if (callbackError) {
                callbackError(error);
            } else {
                console.log(error.message);
                console.log(error);
            }
        }

    }
};

export default AutenticacionController;