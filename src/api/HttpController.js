let Http = {

    obtenerHeaders: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'app': "AppDeTest",
        'version': '1.0.0',
        'build': 1
    },

    initGET: function (token) {
        return this.init('GET', token);
    },

    initPOST: function (token, data) {
        return this.init('POST', token, data);
    },

    initDELETE: function (token, data) {
        return this.init('DELETE', token, data);
    },

    init: function (method, token, data) {

        let init = {
            method: method,
            credentials: 'include',
            headers: this.obtenerHeaders
        };

        if (token) {
            init.headers.csrf = token;
        }

        if (data) {
            init.body = JSON.stringify(data);
        }

        return init;
    },

    errorHandler: function (response, callback) {
        if (response.status === 200) {
            return response.json();
        } else {
            let error = "http error " + response.status + ', ' + response.url;
            if (response.status === 404) {
                error = 'Server no encontrado, ' + response.url;
            } else if (response.status === 403) {
                error = 'Acceso denegado.';
            }
            callback(error);
        }
    }

};

export default Http;