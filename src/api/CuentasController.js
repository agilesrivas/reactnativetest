import {AsyncStorage} from "react-native";
import Http from "./HttpController";

let CuentasController={
    agregarPHPSESSID: async function (url) {

        let sessid = await AsyncStorage.getItem("PHPSESSID");

        if (sessid) {
            url += (url.includes('?') ? '&' : '?') + sessid;
        } else {
            console.log('no hay PHPSESSID para agregar');
        }

        return url;

    },
    obtenerCuentasGrupos: async function (page,
                                          limit,
                                          callback,
                                          callbackError) {

        let url = 'http://192.168.2.215//app-avl/api/cuentasGrupos.php';

        url += '?pagina=' + page;
        url += '&cantidad=' + limit;


        let agruparConfig = 0;
        let orderBy = 1;
        url += '&agrupar=' + agruparConfig;
        url += '&orderBy=' + orderBy;


        AsyncStorage.getItem("csrf").then(async (token) => {

            if (token) {
                url = await this.agregarPHPSESSID(url);
                fetch(url, Http.initGET(token))
                    .then((response) => Http.errorHandler(response, callbackError))
                    .then(callback)
                    .catch(callbackError);

            } else {
                callbackError('token invalido');
            }
        });

    }
};

export default CuentasController;