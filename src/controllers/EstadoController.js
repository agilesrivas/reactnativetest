const Estado = {};
const mensajeError = {
    mensaje: "Se desmonto el componente,sera cancelado el proceso",
    seCancelo: true
};
export default class EstadoController {

    constructor(state) {
        this.estado = state;
        this.promesasPendientes = [];
        this.cancel = false;
    }

    /**
     * @Comprobacion
     * Comprueba el tipo
     * de dato que debe ser un Objeto.
     * */
    esObjeto = (estado) => {
        let valido = false;
        if (typeof estado === "object") {
            valido = true;
        }
        return valido;
    };
    /**
     * @Comprobacion
     * Comprueba el tipo
     * de dato que debe ser una Funcion.
     * */
    esFuncion = (estado) => {
        let valido = false;

        if (typeof estado === "function") {
            valido = true;
        }
        return valido;
    };

    /**
     * @Promesas*/
    existePromesa = (promesa) => {
        let promesaValida = true;

        if (promesa === undefined) {
            promesaValida = false;
        }

        if (promesa === null) {
            promesaValida = false;
        }

        if (promesa === "") {
            promesaValida = false;
        }
        return promesaValida;
    };
    agregarPromesa = (promesa) => {
        this.promesasPendientes.push(promesa);
    };
    removerPromesa = (promesa) => {
        this.promesasPendientes = this.promesasPendientes.filter(
            (p) => {
                return p !== promesa;
            });
    };

    cancelarPromesas() {
        if (this.promesasPendientes !== null &&
            this.promesasPendientes.length > 0) {
            this.promesasPendientes.map((p, index) => {
                p.cancel()
            });
            this.promesasPendientes = [];
        }
        this.cancel = true;
    };

    hacerPromesaCancelable = (promesa) => {
        let seCancela = false;

        const promesaCancelable = new Promise((resolve, reject) => {
            if (seCancela) {
                reject(mensajeError);
            } else {
                promesa.then(
                    val => seCancela ? reject(mensajeError) : resolve(val),
                    error => seCancela ? reject(mensajeError) : reject(error)
                );
            }
        });

        const promesaEjecutable = {
            usarPromesa: promesaCancelable,
            cancel() {
                seCancela = true;
            },
        };
        this.agregarPromesa(promesaEjecutable);

        return promesaEjecutable;

    };
    usarPromesa = async (promesa) => {

        try {
            const promesaCancelable = this.hacerPromesaCancelable(
                promesa
            );

            const data = await promesaCancelable.usarPromesa;

            this.removerPromesa(promesaCancelable);

            return data;
        } catch (error) {
            console.log(error);
        }
    };

    /**
     * @Estado*/


    /**
     * @Object.keys
     * siempre retornara un array con los
     * indices del objeto
     * por parametro.
     * */
    existeEstado = (estado) => {
        let estadoValido = true;

        if (estado === undefined) {
            estadoValido = false;
        }

        if (estado === null) {
            estadoValido = false;
        }

        if (estado === "") {
            estadoValido = false;
        }
        if (!this.esFuncion(estado)) {
            const indices = Object.keys(estado);

            if (indices.length === 0) {
                estadoValido = false;
            }
        }


        return estadoValido;
    };
    inicializarEstado = (estado) => {
        const existeEstado = this.existeEstado(estado);
        const existeEstadoPrevio = this.existeEstado(this.estado.state);
        if (existeEstado && !existeEstadoPrevio) {
            console.log("Estado Inicializado");
            this.setEstado(estado);
        } else {
            console.log("Estado ya Inicializado");
            this.setEstado((prev) => ({
                ...prev,
                ...estado
            }));
        }
    };
    obtenerDiferenciaDeEstado = (estado) => {
        let indices = null;

        const state = this.estado.state;

        if (this.existeEstado(estado)) {
            indices = Object.keys(estado);
            let estadoDiferente = {};
            indices.map((indice) => {
                if (state[indice] === undefined) {
                    estadoDiferente = {
                        ...estadoDiferente,
                        [indice]: estado[indice]
                    };
                }
                if (Array.isArray(state[indice])) {
                    /**
                     * ¿ Verificar Si son distintos ?*/
                    estadoDiferente = {
                        ...estadoDiferente,
                        [indice]: estado[indice]
                    }
                } else {
                    if (state[indice] !== estado[indice]) {
                        estadoDiferente = {
                            ...estadoDiferente,
                            [indice]: estado[indice]
                        }
                    }
                }

            });

            if (!this.existeEstado(estadoDiferente)) {
                return {};
            }
            return estadoDiferente;

        }
    };
    obtenerEstado = (key = "") => {
        let estado = null;
        if (key !== "") {
            estado = this.estado.state[key];
        }
        estado = this.estado.state;

        if (estado === undefined) {
            estado = {};
        }
        return estado;
    };
    obtenerProps = (key = "") => {
        let props = null;
        if (key !== "") {
            props = this.estado.props[key];
        }
        props = this.estado.props;

        if (props === undefined) {
            props = {};
        }
        return props;
    };
    obtenerParametrosDeNavegacion = (key) => {
        let parametros = {};
        let props = this.estado.props;
        let navegacion = props.navigation;
        let estadoNavegacion = navegacion.state;
        let parametrosNavegacion = estadoNavegacion.params;
        if (parametrosNavegacion !== undefined && parametrosNavegacion !== null) {
            if (key !== "") {
                parametros = parametrosNavegacion[key];
            }
            parametros = parametrosNavegacion;

            if (parametros === undefined) {
                parametros = {};
            }
        }

        return parametros;
    };
    setEstado = (estado, callback = {}) => {
        let state = null;
        const existeEstado = this.existeEstado(estado);
        if (existeEstado) {
            if (this.esObjeto(estado)) {
                state = this.obtenerDiferenciaDeEstado(estado);
            }
            if (this.esFuncion(estado)) {
                state = this.obtenerDiferenciaDeEstado(estado());
            }
            if (!this.esFuncion(callback)) {
                callback = () => {
                };
            }

            if (this.existeEstado(state)) {
                if (!this.cancel) {
                    const promesa = this.hacerPromesaCancelable(
                        new Promise((resolve) => {
                            this.estado.setState(state, callback);
                        })
                    );
                    promesa.usarPromesa.then();
                    this.removerPromesa(promesa);
                }
            }
        }
    };
};