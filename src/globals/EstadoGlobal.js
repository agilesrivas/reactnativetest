import React, {Component} from "react";
import EstadoController from "../controllers/EstadoController";

export default class EstadoGlobal extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.estadoController = new EstadoController(this);
    }

    componentWillUnmount() {
        this.estadoController.cancelarPromesas();
    }
};