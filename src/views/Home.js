import React, {Component} from 'react';
import EstadoGlobal from '../globals/EstadoGlobal';
import {StyleSheet, Text, View, SectionList, Button, TextInput} from "react-native";

export default class Home extends EstadoGlobal {

    constructor(props) {
        super(props);
        this.count = 0;
        /**
         * Prueba de inicializarlo*/
        // this.state={
        //     contador:1
        // }
    }

    componentDidMount() {
        /**Si se inicializa antes usa ese estado y el nuevo que se le otorga*/
        /**Si no se inicializa antes se usa el nuevo estado*/
        this.estadoController.inicializarEstado({
            sections: [
                {id: 1, title: 'JEFES', data: []},
                {id: 2, title: 'DESARROLLO', data: []},
                {id: 3, title: 'SOPORTE', data: []},
            ]
        });
    }

    componentWillUnmount() {
        console.log("-----------------------------");

        console.log("--->Componente Desmontado<---");

        console.log("-----------------------------");

        this.estadoController.cancelarPromesas();
    }


    agregarPersona = (idSeccion) => {
        const {sections, nombre} = this.state;
        let secciones = sections;
        this.count += 1;
        const nombrePersona = (nombre !== null && nombre !== "") ? nombre : "Vacio";
        const persona = {id: this.count, nombre: nombrePersona};

        secciones.map((section) => {
            if (section.id === idSeccion) {
                section.data.push(persona);
            }
        });


        this.estadoController.setEstado(
            (prev) => ({
                sections: secciones,
                mensaje: `Has creado correctamente ${persona.nombre}!`
            }),
            () => {
                alert(this.state.mensaje);
            }
        );

    };
    renderHeader = () => {
        return (
            <View>
                <Button onPress={
                    () => {
                        this.props.navigation.navigate('EstadoHome', {id: '10000805'})
                    }
                } title={'Ir a otra Pantalla'}/>

            </View>
        )
    };
    renderItem = ({item}) => {
        return <Text style={styles.item}>{item.nombre}</Text>;
    };
    renderSectionHeader = ({section}) => {
        return (
            <View>
                <Text style={styles.sectionHeader}>{section.title}</Text>
                <TextInput
                    key={section.id}
                    placeholder={section.title}
                    onChangeText={(e) => {
                        this.estadoController.setEstado({nombre: e});
                    }}
                />
                <Button
                    onPress={() => {
                        this.agregarPersona(section.id);
                    }} title={"Agregar"}/>
            </View>
        );
    };


    render() {
        const {sections} = this.state;

        console.log("------------------------");

        console.log("--->Render<---");

        console.log("-------------------------");


        return (
            <View style={styles.container}>
                {
                    sections &&
                    <SectionList
                        sections={sections}
                        renderItem={this.renderItem}
                        ListHeaderComponent={this.renderHeader}
                        renderSectionHeader={this.renderSectionHeader}
                        keyExtractor={(item, index) => item + index}
                    />
                }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 22
    },
    sectionHeader: {
        paddingTop: 2,
        paddingLeft: 10,
        paddingRight: 10,
        paddingBottom: 2,
        fontSize: 14,
        fontWeight: 'bold',
        backgroundColor: 'rgba(247,247,247,1.0)',
    },
    item: {
        padding: 10,
        fontSize: 18,
        height: 44,
    },
});