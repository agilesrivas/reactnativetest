import React, {Component} from "react";
import EstadoGlobal from "../globals/EstadoGlobal";
import {Text, StyleSheet, View, Button, ScrollView} from "react-native";
import Axios from "axios";
import AutenticacionController from "../api/AutenticacionController";
import CuentasController from "../api/CuentasController";

export default class EstadoHome extends EstadoGlobal {
    constructor(props) {
        super(props);
    }

    async componentDidMount() {

        //await this.apiTest();

        await this.estadoController.usarPromesa(AutenticacionController.CSRF());
        await CuentasController.obtenerCuentasGrupos(
            0,
            500,
            this.onCuentaGrupos,
            this.onCuentaGruposError
        );

        this.estadoController.setEstado({
            nombre:'default',
            contador: 1
        });

        this.estadoController.setEstado({
            contador: 1
        });
    }

    onCuentaGrupos = (response) => {
        this.estadoController.setEstado({cuentaGrupos: response.grupos});
    };
    onCuentaGruposError = (error) => {
        console.log(error);
    };
    apiTest = async () => {
        try {
            // const url = "https://pokeapi.co/api/v2/pokemon/?offset=0&limit=1000";
            // let response = await this.estadoController.usarPromesa(
            //     Axios.get(url)
            // );
            // let pokemons = response.data.results;

            this.estadoController.setEstado({
                nombre: "Alejandro",
                contador: 4
            });

        } catch (err) {
            console.log(err);
        }

    };

    componentWillUnmount() {
        console.log("-----------------------------");

        console.log("--->Componente Desmontado<---");

        console.log("-----------------------------");


        this.estadoController.cancelarPromesas();
    }


    render() {
        const {data, cuentaGrupos, nombre, contador} = this.estadoController.obtenerEstado();
        const {id} = this.estadoController.obtenerParametrosDeNavegacion();
        console.log("------------------------");

        console.log("--->Render<---");

        console.log("-------------------------");
        return (
            <View style={styles.container}>
                <Button onPress={this.apiTest} title={"Invocar"}/>
                <Text>
                    {nombre} : {contador}
                </Text>
                <ScrollView>
                    {
                        data &&
                        data.map((c, index) => {
                            return (

                                <Text key={c.name + index.toString()} style={{textAlign: 'center'}}>
                                    {c.name}
                                </Text>
                            );
                        })
                    }
                </ScrollView>
            </View>
        );
    }

};
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignContent: 'center'
    },

});